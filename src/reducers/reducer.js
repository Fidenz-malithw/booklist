import { combineReducers } from 'redux';
import BooksReducer from './books.reducer';
import ActiveReducer from './activebook.reducer';

const rootReducer = combineReducers({
  books: BooksReducer,
  activeBook: ActiveReducer
});

export default rootReducer;