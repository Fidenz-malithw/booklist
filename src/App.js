import React, { Component } from 'react';
import { Provider } from 'react-redux';
import { createStore } from 'redux';
import Reducers from './reducers/reducer';
import './App.css';
import 'bootstrap/dist/css/bootstrap.min.css';
import BookList from './containers/bookList/bookList.container';
import BookDetail from './containers/bookdetail.container';

let store = createStore(Reducers);

class App extends Component {
  render() {
    return (
      <Provider store={store}>
        <div className="container">
          <BookList />
          <BookDetail />
        </div>
      </Provider>
    );
  }
}

export default App;
